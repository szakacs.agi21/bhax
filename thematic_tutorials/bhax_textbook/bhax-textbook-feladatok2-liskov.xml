<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Liskov helyettesítés sértése</title>
        <para>
            rjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a megoldásra: jobb OO tervezés. <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf">93-99 fólia</link> 
        </para>
        <para>
            <link xlink:href="source/binom/Batfai-Barki/madarak/">source/binom/Batfai-Barki/madarak/</link>       
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/szakacs.agi21/bhax/-/blob/master/thematic_tutorials/bhax_textbook/Liskov/liskovrafigyel.cpp">liskovrafigyel.cpp,</link>
            <link xlink:href="https://gitlab.com/szakacs.agi21/bhax/-/blob/master/thematic_tutorials/bhax_textbook/Liskov/liskovsert.cpp">liskovsert.cpp</link>                 
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            A Liskov-féle behelyettesítési elv kimondja, hogy a program viselkedése nem változhat meg attól, hogyaz ős osztály egy példánya helyett a jövőben valamelyik gyermek osztályának példányát használom. Ahivatalos megfogalmazása így szól: ha S altípusa T-nek, akkor minden olyan helyen ahol T-t felhasználjukS-t is minden gond nélkül behelyettesíthetjük anélkül, hogy a programrész tulajdonságai megváltoznának. Ez viszont nem mindig igaz.
        </para>
        <para>
            Klasszikus példa a Liskov helyettesités sértésre az ellipszis – kör illetve a téglalap – négyzet példa. Akör olyan speciális ellipszis, ahol a két sugár egyenlő. A négyzet olyan speciális téglalap, ahol az oldalakegyenlő hosszúak. Logikusan következik, hogy a kör az ellipszis alosztálya, a négyzet pedig a téglalapalosztálya legyen. De mi van akkor, ha egy olyan függvényt készítünk, amely az ellipszis tengelyeit ismódosítja? Akkor a kör megszünik körnek lenni és itt látjuk, a problémát.
        </para>
        <para>
           Én viszont nem ezt az esetet részlezném hanem a feladatleírásban megadott linkben lévőt.
        </para>
        <programlisting language="c++"><![CDATA[
            // ez a T az LSP-ben
            class Madar {
            public:
                 virtual void repul() {};
            };

            // ez a két osztály alkotja a "P programot" az LPS-ben
            class Program {
            public:
                 void fgv ( Madar &madar ) {
                      madar.repul();
                 }
            };

            // itt jönnek az LSP-s S osztályok
            class Sas : public Madar
            {};

            class Pingvin : public Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
            {};

            int main ( int argc, char **argv )
            {
                 Program program;
                 Madar madar;
                 program.fgv ( madar );

                 Sas sas;
                 program.fgv ( sas );

                 Pingvin pingvin;
                 program.fgv ( pingvin ); // sérül az LSP, mert a P::fgv röptetné a Pingvint, ami ugye lehetetlen.

            }
        ]]></programlisting>
        <para>
            Íme a megoldás:
        </para>
        <programlisting language="c++"><![CDATA[
            // ez a T az LSP-ben
            class Madar {
            //public:
            //  void repul(){};
            };

            // ez a két osztály alkotja a "P programot" az LPS-ben
            class Program {
            public:
                 void fgv ( Madar &madar ) {
                      // madar.repul(); a madár már nem tud repülni
                      // s hiába lesz a leszármazott típusoknak
                      // repül metódusa, azt a Madar& madar-ra úgysem lehet hívni
                 }
            };

            // itt jönnek az LSP-s S osztályok
            class RepuloMadar : public Madar {
            public:
                 virtual void repul() {};
            };

            class Sas : public RepuloMadar
            {};

            class Pingvin : public Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
            {};

            int main ( int argc, char **argv )
            {
                 Program program;
                 Madar madar;
                 program.fgv ( madar );

                 Sas sas;
                 program.fgv ( sas );

                 Pingvin pingvin;
                 program.fgv ( pingvin );

            }
            }
        ]]></programlisting>
    </section>        

    <section>
        <title>Szülő-gyerek</title>
        <para>
            Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön keresztül csak az ős üzenetei küldhetőek! Lásd: <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf">98 fólia</link>   
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/szakacs.agi21/bhax/-/blob/master/thematic_tutorials/bhax_textbook/Liskov/Szulo-Gyerek.cpp">Szulo-Gyerek.cpp,</link> <link xlink:href="https://gitlab.com/szakacs.agi21/bhax/-/blob/master/thematic_tutorials/bhax_textbook/Liskov/Szulo-Gyerek.java">Szulo-Gyerek.java</link>                
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            Szülő-gyerek java:
        </para>
        <programlisting language="java"><![CDATA[
            class Szulo
            {
                public void viselkedesszulo()
                {
                    System.out.println("Szülő viselkedés");
                }
            }

            class Gyerek extends Szulo
            {
                public void viselkedesgyerek()
                {
                    System.out.println("Gyerek viselkedés");
                }
            }

            class szulogyerek
            {
                
                public static void main(String[] args)
                {
                    Szulo szulo = new Gyerek();
                    szulo.viselkedesszulo();
                    //szulo.viselkedesgyerek();
                }
            }
        ]]></programlisting>
        <para>
            A Szulo osztályunk szármoztatott alosztálya a Gyerek osztály. A main-be létrehozunk egy Szulo tipusúszulo változót. Ennek a változónak először meghívjuk a viselkedésszulo függvényét ami rendben le is fut. Majd ha a viselkedesgyerek függvényt hívjuk, hibaüzenetet kapunk a fordításnál mivel, míg a gyerek ismeri a szulo adatait, addig ez fordítva nem igaz.
        </para>
        <para>
            A program kimentele, ha kikomenteljük a szulo.viselkedesgyerek(); függvényt, valamint ha nem:
        </para>
        <mediaobject>
            <imageobject>
            <imagedata fileref="Prog2/szulogyerekjavafut.png" scale="45" />
            </imageobject>
            </mediaobject>

            <mediaobject>
            <imageobject>
            <imagedata fileref="Prog2/szulogyerekjavanemfut.png" scale="45" />
            </imageobject>
            </mediaobject>
            <para>
                szülő-gyerek c++:
            </para>
            <programlisting language="c++"><![CDATA[
            #include<iostream>
            using namespace std;

            class Szulo
            {
                public:
                void szviselkedes()
                {
                    cout << "Szülő viselkedés" << endl;
                }
            };

            class Gyerek : public Szulo
            {
                virtual void gyviselkedes()
                {
                    cout << "Gyerek viselkedés" << endl;
                }
            };

            int main(int argc, char* argv[])
            {
                Szulo szulo;
                szulo.gyviselkedes(); 
                //szulo.szviselkedes();
            }
        ]]></programlisting>
        <para>
            A program kimentele, ha kikomenteljük a szulo.gyviselkedes(); függvényt, valamint ha nem:
        </para>
        <mediaobject>
            <imageobject>
            <imagedata fileref="Prog2/szulogyerekcppfut.png" scale="45" />
            </imageobject>
            </mediaobject>
            <mediaobject>
            <imageobject>
            <imagedata fileref="Prog2/szulogyerekcppfut.png" scale="45" />
            </imageobject>
            </mediaobject>
            <para>
                Láthatjuk, hogy mind a két program, jól működik, hisz mikor kivesszük a szulo.szviselkedes(); sort, akkor lefut, hisz a szulo.gyviselkesés(); végre tudja hajtani, viszont ha bennehagyjuk akkor hibát ír ki.
            </para>
    </section>       

    <section>
        <title>Ciklomatikus komplexitás</title>
        <para>
            Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom tekintetében a <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf">93-99 fólia</link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/szakacs.agi21/bhax/-/blob/master/thematic_tutorials/bhax_textbook/Liskov/CyclomaticComplexityDemo.java">CyclomaticComplexityDemo.java</link>                
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            A ciklomatikus komplexitás egy szoftvermetrika, melyet Thomas J. McCabe publikált 1976-ban. A metrika egy adott szoftver forráskódjának alapján határozza meg annak komplexitását, egy konkrét számértékben kifejezve. A komplexitás számítása a gráfelméletre alapul. A forráskódban az elágazásokból felépülő gráf pontjai, és a köztük lévő élek alapján számítható az alábbi módon:
        </para>
        <para>
            A ciklomatikus (McCabe) komplexitás értéke:
        </para>
        <para>
            M = E - N + 2P
        </para>
        <para>
            ahol
        </para>
        <para>
            E: A gráf éleinek száma
        </para>
        <para>
            N: A gráfban lévő csúcsok száma
        </para>
        <para>
            P: Az összefüggő komponensek száma
        </para>
        <para>
            A ciklomatikus szám: M = E - N + P
        </para>
        <para>
            forrás: <link xlink:href="https://hu.wikipedia.org/wiki/Ciklomatikus_komplexit%C3%A1s">https://hu.wikipedia.org/wiki/Ciklomatikus_komplexit%C3%A1s</link>  
        </para>
        <programlisting language="java"><![CDATA[
            public class CyclomaticComplexityDemo {
                public static void main(String[] args) {
                    int var1 = 10;
                    int var2 = 9;
                    int var3 = 8;
                    int var4 = 7;
                    if (var1 == 10){
                        if(var2 > var3){
                            var2 = var3;
                        }
                        else{
                            if (var3 > var4){
                                var3 = var4;
                            }
                        else{
                            var4 = var1;
                            }
                        }
                    }
                    else{
                        var1=var4;
                    }
                    System.out.println("Printing value for var1, var2, var3, and var4"+var1+" "+var2+" "+var3+" "+var4);
                }
            }
        ]]></programlisting>
        <para>
            A fenti program ciklomatikus komplexitását láthatjuk az alábbi képen:
        </para>
            <mediaobject>
            <imageobject>
            <imagedata fileref="Prog2/Ciklomatikus-komplexitas.png" scale="45" />
            </imageobject>
            </mediaobject>
            <para>
                A ciklomatikus komplexitás kiszámításához használt oldal: <link xlink:href="http://www.lizard.ws/">http://www.lizard.ws/</link>  
            </para>
    </section>                                                                           
</chapter>                
