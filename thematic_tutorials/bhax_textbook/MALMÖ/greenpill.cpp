// Malmo:
#include <AgentHost.h>
#include <ClientPool.h>
using namespace malmo;

// STL:
#include <cstdlib>
#include <exception>
#include <iostream>
using namespace std;

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#define delay boost::this_thread::sleep(boost::posix_time::milliseconds
#define ahsc agent_host.sendCommand

AgentHost agent_host;
WorldState world_state;

void felmegy();
void run(boost::property_tree::ptree);
void nbrindex(int);
bool csapda(string[]);
bool inventory(int, int);
void szintvalt(string[], int);
void keres(string[]);
bool lava(string[]);
bool falelottem(string[]);
void tamadas();

string nbr[27];
int y = 0;
int yaw = 0;

int elottem = 0;
int ebalra = 0;
int ejobbra = 0;
int mogottem = 0;
int mbalra = 0;
int mjobbra = 0;
int jobbra = 0;
int balra = 0;
int virag = 0;
int seged = 1;

int n = 41;
int max = 0;

int main(int argc, const char **argv)
{
    try
    {
        agent_host.parseArgs(argc, argv);
    }
    catch( const exception& e )
    {
        cout << "ERROR: " << e.what() << endl;
        cout << agent_host.getUsage() << endl;
        return EXIT_SUCCESS;
    }
    if( agent_host.receivedArgument("help") )
    {
        cout << agent_host.getUsage() << endl;
        return EXIT_SUCCESS;
    }

    std::ifstream xmlf{"nb4tf4i_d.xml"};
    std::string xml{std::istreambuf_iterator<char>(xmlf), std::istreambuf_iterator<char>()};

    MissionSpec my_mission{xml, true};
   
    MissionRecordSpec my_mission_record("./saved_data.tgz");

    int attempts = 0;
    bool connected = false;
    do {
        try {
            agent_host.startMission(my_mission, my_mission_record);
            connected = true;
        }
        catch (exception & e) {
            cout << "Error starting mission: " << e.what() << endl;
            attempts += 1;
            if (attempts >= 3)
                return EXIT_FAILURE;    // Give up after three attempts.
            else
                boost::this_thread::sleep(boost::posix_time::milliseconds(1000));   // Wait a second and try again.
        }
    } while (!connected);

    cout << "Waiting for the mission to start" << flush;
    do {
        cout << "_" << flush;
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        world_state = agent_host.getWorldState();
    } while (!world_state.has_mission_begun);
    cout << endl;

    for (int kezd = 1; kezd <= 5; kezd++)
        ahsc("move 1");
        delay(140));

    felmegy();

    ahsc("move 1");

    // main loop:
    do {
        
        /*for (std::vector<boost::shared_ptr<TimestampedString>>::iterator it =
        world_state.observations.begin();
        it!= world_state.observations.end(); ++it)
        {
        boost::property_tree::ptree pt;
        std::istringstream iss((*it)->text);
        boost::property_tree::read_json(iss, pt);
        std::string lookingAt = pt.get<std::string>("lineOfSight.type");
        if (lookingAt == "red_flower")
        {
            tamadas();
            ahsc("attack 1");
            delay(140));
        }
        }*/

        ahsc("turn -1");
        delay(200));
        
        world_state = agent_host.getWorldState();

    } while (world_state.is_mission_running);

    cout << "Mission has stopped." << endl;

    return EXIT_SUCCESS;
}

//FGV-ek
void run(boost::property_tree::ptree pt)
{
    y = stoi(pt.get<string>("YPos"));
    yaw = stoi(pt.get<string>("Yaw"));
    int i=-1;
    for (auto& e : pt.get_child("nbr3x3"))
    {
        ++i;
        nbr[i] = e.second.get<string>("");
    }
    nbrindex(yaw);

}
void nbrindex(int yaw)
{
    if (yaw == 180)
    {
        ebalra = 0;
        elottem = 1;
        ejobbra = 2;
        balra = 3;
        jobbra = 5;
        mbalra = 6;
        mogottem = 7;
        mjobbra = 8;
    } else if (yaw == 270)
    {
        ebalra = 2;
        elottem = 5;
        ejobbra = 8;
        balra = 1;
        jobbra = 7;
        mbalra = 0;
        mogottem = 3;
        mjobbra = 6;
    } else if (yaw == 0)
    {
        ebalra = 8;
        elottem = 7;
        ejobbra = 6;
        balra = 5;
        jobbra = 3;
        mbalra = 2;
        mogottem = 1;
        mjobbra = 0;
    } else
    {
        ebalra = 6;
        elottem = 3;
        ejobbra = 0;
        balra = 7;
        jobbra = 1;
        mbalra = 8;
        mogottem = 5;
        mjobbra = 2;
    }
}

void felmegy()
{
    for (y = 0; y < n; ++y)
    {
        ahsc("move 1");
        delay(140));
        ahsc("jumpmove 1");
        delay(140));
    }
}