#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

int f(int x, int y){
	printf("F-ben %d %d\n", x, y);
	return x + y;
}

int g(int x){
	printf("G-ben %d\n", x);
	x *= 2;
	return x;
}

int h(int* x){
	printf("H-ban %d\n", *x);
	*x *= 2;
	printf("H-ban 2 %d\n", *x);
	return *x;
}

void jelkezelo(){
	printf("\nJel érkezett\n");
	exit(0);
}

int main()
{

	if(signal(SIGINT, jelkezelo)==SIG_IGN)
		signal(SIGINT, SIG_IGN);

	int tomb[10];
	int dt[40], st[40];
	int *s = st;
	int *d = dt;
	int n = 40;
	int a = 42;

	int i = 0;
	for(i = 0; i < 10; i++)
		printf("%d\n", i);

	/*
	* for c99 example
	for(int j = 0; j < 10; j++)
		printf("%d\n", j);
	*/

	for(i = 0; i < 10; tomb[i] = i++)
		printf("%d\n", tomb[i]);

	for(i = 0; i < n && (*d++ = *s++); i++)
		printf("%d\n", i);

	printf("F-en kívül: %d %d\n", f(a, ++a), f(++a, a));

	printf("G-n kívül: %d %d\n", g(a), a);

	printf("H-n kívül: %d %d\n", h(&a), a);

	for(;;){}

	return 0;
}



