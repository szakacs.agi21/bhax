 kiserletek_szama=100
    nyeremenyszam = sample(1:3, kiserletek_szama, replace=T)
    jatekos = sample(1:3, kiserletek_szama, replace=T)
    musorvezeto=vector(length = kiserletek_szama)

    for (i in 1:kiserletek_szama) { 

        if(nyeremenyszam[i]==jatekos[i]){
    
        mibol=setdiff(c(1,2,3), nyeremenyszam[i])
    
        }else{
    
        mibol=setdiff(c(1,2,3), c(nyeremenyszam[i], jatekos[i]))
    
        }

    musorvezeto[i] = mibol[sample(1:length(mibol),1)]

    }

    nemvaltoztatesnyer= which(nyeremenyszam==jatekos)
    valtoztat=vector(length = kiserletek_szama)

    for (i in 1:kiserletek_szama) {

    holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
    valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
    
    }

    valtoztatesnyer = which(nyeremenyszam==valtoztat)

    sprintf("Kiserletek szama: %i", kiserletek_szama)
    length(nemvaltoztatesnyer)
    length(valtoztatesnyer)
