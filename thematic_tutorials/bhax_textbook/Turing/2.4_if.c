#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr (); //lekérdezi a terminál adatait és kitöröl mindent róla

    int x = 0;
    int y = 0;

    int ynov =3;    //hanyassával vegye a labda a sorokat
    int xnov = 6;   //hanyassával vegye a labda az oszlopokat

    int my;  //sorok száma    
    int mx;  //oszlopok száma

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );

  mvprintw ( y, x, "O" );

        refresh ();
        usleep ( 100000 );
        clear(); //egy labda fog pattogni

        x = x + xnov;
        y = y + ynov;

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        if ( y<=0 ) { // elerte-e a tetejét?
            ynov = ynov * -1;
        }

   if ( y>=my-1 ) { // elerte-e az alját?
            ynov = ynov * -1;
        }

    }

    return 0;
}